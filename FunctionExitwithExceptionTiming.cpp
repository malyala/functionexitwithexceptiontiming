/***********************************************************
  Name: FunctionexitwithExceptionsTiming.cpp
  Author: Amit Malyala                                     
  Copyright: All rights reserved, Amit Malyala, 2020
   Description:
   This function either returns a value or that throws that value based on an argu- ment. Measure the difference in run-time between the two ways.    
   Notes:
   The difference between the two is about 3300 ns with function return by value faster than function exit with exception by 3 times.
   
   Version and bug history:
   0.1 Initial version.  
   Coding log:
   13-11-20 created initial version.
***********************************************************/
#include <iostream>

#include <chrono>

#include <cstdlib>

// Function prototypes of function one which takes a argument
int Functionone(int level);
/* Application */
void ExecuteApp(void);

/* Function main */
int main(void) {
    ExecuteApp();
    return 0;
}
/* Function return */
int Functionone(int level) {
    if (level % 3) {
        throw level;
    }
    return level * 4 / 6;
}

/* Application */
void ExecuteApp(void) {
    long int f1sum = 0;
    constexpr int NumberofEntries = 15;
    bool PrintedFunctionReturnTiming = false;
    bool PrintedFunctionExceptionTiming = false;
    bool PrintedBoth = false;
    long int FunctionReturnSum = 0;
    long int FunctionExceptionSum = 0;
    int FunctionReturnCount = 0;
    int FunctionExceptionCount = 0;
    long int FunctionReturnTiming = 0;
    long int FunctionExceptionTiming = 0;
    long int diff = 0;
    long int f1returnvaluesum = 0;
    long int f1exceptionvaluesum = 0;

    std::cout << "This function calculates timing of funtion return value or exception thrown out of it based on argument to that function.\n";
    std::cout << "We will measure " << NumberofEntries << " different timings in nanoseconds.\n";
    for (int index = 0; index < NumberofEntries; index++) {
        for (int i = 0; i < 3000 && (!PrintedBoth); i++) {
            // Measure block execution time.
            auto start = std::chrono::steady_clock::now();
            try {

                if (!PrintedFunctionReturnTiming) {
                    // The return value does something
                    f1sum += Functionone(i);
                    auto end = std::chrono::steady_clock::now();
                    FunctionReturnTiming = std::chrono::duration_cast < std::chrono::nanoseconds > (end - start).count();
                    FunctionReturnSum += FunctionReturnTiming;
                    FunctionReturnCount++;
                }
            } catch (int val) {

                if (!PrintedFunctionExceptionTiming) {
                    auto end = std::chrono::steady_clock::now();
                    FunctionExceptionTiming = std::chrono::duration_cast < std::chrono::nanoseconds > (end - start).count();
                    FunctionExceptionSum += FunctionExceptionTiming;
                    FunctionExceptionCount++;

                }
            }

            if (FunctionReturnCount == 1000 && (!PrintedFunctionReturnTiming)) {
                std::cout << "Function return timing: " << FunctionReturnSum / 1000 << "\n";
                FunctionReturnCount = 0;
                PrintedFunctionReturnTiming = true;
                f1sum = 0;

            }

            if (FunctionExceptionCount == 1000 && (!PrintedFunctionExceptionTiming)) {
                std::cout << "Function exception timing: " << FunctionExceptionSum / 1000 << " \n";
                FunctionExceptionCount = 0;
                PrintedFunctionExceptionTiming = true;
            }

            if ((PrintedFunctionReturnTiming) && (PrintedFunctionExceptionTiming)) {
                PrintedBoth = true;
                PrintedFunctionReturnTiming = false;
                PrintedFunctionExceptionTiming = false;
                f1exceptionvaluesum += FunctionExceptionSum;
                f1returnvaluesum += FunctionReturnSum;
                FunctionReturnSum = 0;
                FunctionExceptionSum = 0;
            }

            if (PrintedBoth) {
                diff += std::abs(FunctionReturnTiming - FunctionExceptionTiming);
            }
        }
        PrintedBoth = false;

    }
    f1exceptionvaluesum /=  NumberofEntries;
    f1returnvaluesum /= NumberofEntries;
    std::cout << "Difference between execution times is about " << diff / NumberofEntries << " nanoseconds\n";
    if (f1returnvaluesum < f1exceptionvaluesum) {
        std::cout << "Function return value is about " << (float) f1exceptionvaluesum / f1returnvaluesum << " times faster than exception value returned from function\n";
    } else {
        std::cout << "Exception from function is about " << (float) f1returnvaluesum / f1exceptionvaluesum << " times faster than value returned from function\n";
    }
    f1returnvaluesum = 0;
    f1exceptionvaluesum = 0;
    diff = 0;
}

#if(0)
// Measure function execution time.
//auto start = std::chrono::steady_clock::now();

// Measure execution time.
//auto end = std::chrono::steady_clock::now();
//auto diff = std::chrono::duration_cast < std::chrono::nanoseconds > (end - start).count();
//std::cout << "Execution time " << diff << " nanoseconds";
#endif